type CarYear = number
type CarType = string
type CarModel = string
type Car = {
  year: CarYear,
  type: CarType,
  model: CarModel
}

const carYear: CarYear = 2022
const carType: CarType = "Honda"
const carModel: CarModel = "City"
const theCar: Car = {
  year: carYear,
  type: carType,
  model: carModel
};

console.log(theCar);