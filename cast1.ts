let xx: unknown = 'hello';
let num: string = '1';

console.log((xx as string).length);
console.log((num as unknown) as number);