const car: { type: string, model: string, year?: number } = {
    type: "Toyota",
    model: "Corolla",
    year: 2009
  };

  console.log(car);
  console.log(car.type);
  console.log(car.model);