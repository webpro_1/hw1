function printHello(): void {
    console.log('Hello!');
  }

  function multiply(a: number, b: number):number {
    return a * b;
  }
  function addNum (a:number, b: number, c?:number):number {
    return a + b+ (c || 0);

  }
  console.log(multiply(1, 2));
  console.log(addNum(1,2,3));
  console.log(addNum(1,2));

  function pow(value: number, exponent: number = 10) {
    return value ** exponent;
  }

  console.log(pow(10));
  console.log(pow(10, 1));

  function divide({ dividend, divisor }: { dividend: number, divisor: number }) {
    return dividend / divisor;
  }
  console.log(divide({dividend: 10, divisor: 2}));

  function add(a: number, b: number, ...rest: number[]) {
    return a + b + rest.reduce((p, c) => p + c, 0);
  }
  
  console.log(add(10,10,10,10,10));


  type Negate = (value: number) => number;
const negateFunction: Negate = (value) => value * -1;
const negateFunction2: Negate = function(value:number):number {
  return value*-1;
};
console.log(negateFunction(1));
console.log(negateFunction2(-5));